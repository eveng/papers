# README #

### Papers for the Course Evidence Engineering ###


- DecisionEnable.pdf Decisions as a service for application centric real time analytics: Presented on Feb 16

- slashdotModeration.pdf 

- AdvancesInLogAnalysis.pdf 

- ChaosMonkey.pdf 

- DevOps.pdf

- Hype_cycle_for_cloud_computing_2011.pdf

- TestingInUncertainty.pdf

- class1.pdf - lecture

- VCSBigData.pdf - lecture

- BigDataLogAnalysis.pdf - presented
	
- BigDataPerspectives.pdf - presented
	
- BigDataPipeline.pdf - presented
	
- BigDataPrototyping.pdf - presented
	
- BigDataVideo.pdf - presented
	
- futureSoftwareEngineering.pdf
	
